package com.spring.test.init;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan("com.spring.test")
@EnableJpaRepositories("com.spring.test.dao")
public class ApplicationConfig {
	@Autowired
	private DataSource dataSource;

	@Autowired
	@Qualifier("props")
	private Properties presistenceProps;

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
		emf.setDataSource(this.dataSource);
		emf.setPackagesToScan("com.spring.test");

		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		emf.setJpaVendorAdapter(vendorAdapter);

		emf.setJpaProperties(this.presistenceProps);

		return emf;
	}

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource rv = new DriverManagerDataSource();
		rv.setDriverClassName("com.mysql.jdbc.Driver");
		rv.setUrl("jdbc:mysql://localhost:3306/spring");
		rv.setUsername("root");
		rv.setPassword("");

		return rv;
	}

	@Bean
	@Autowired
	public PlatformTransactionManager transactionManager(
			EntityManagerFactory factory) {
		JpaTransactionManager txm = new JpaTransactionManager();
		txm.setEntityManagerFactory(factory);
		return txm;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean(name = "props")
	public Properties additionalProperties() {
		Properties config = new Properties();
		config.setProperty("hibernate.hbm2ddl.auto", "create-drop");
		config.setProperty("hibernate.show_sql", "true");

		return config;
	}

}
