package com.spring.test.service;

import java.util.List;

import com.spring.test.model.PersonEntity;

public interface PersonService {
	public PersonEntity savePerson(PersonEntity personEntity);

	public List<PersonEntity> getPersons();
}
