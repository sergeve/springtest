package com.spring.test.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.test.dao.PersonRepository;
import com.spring.test.model.PersonEntity;
import com.spring.test.service.PersonService;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {
	
	@Autowired
	private PersonRepository personRepository;

	@Override
	public PersonEntity savePerson(PersonEntity personEntity) {
		PersonEntity person = personRepository.save(personEntity);
		System.out.println("/t" + person + "added successful");
		return person;
	}

	@Override
	public List<PersonEntity> getPersons() {
		List<PersonEntity> rv = new ArrayList<>();
		rv.addAll(personRepository.findAll());
		return rv;
	}

}
