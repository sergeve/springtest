package com.spring.test.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.test.model.PersonEntity;

public interface PersonRepository extends JpaRepository<PersonEntity, Long> {

	@SuppressWarnings("unchecked")
	public PersonEntity save(PersonEntity personEntity);

	public List<PersonEntity> findAll();
}
