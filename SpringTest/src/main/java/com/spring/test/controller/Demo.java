package com.spring.test.controller;

import org.springframework.stereotype.Component;

@Component
public class Demo {
	public String getMessage(){
		return "from spring";
	}
}
