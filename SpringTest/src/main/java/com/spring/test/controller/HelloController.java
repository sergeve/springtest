package com.spring.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloController {


	@Autowired
	private Demo demo;
	
	@RequestMapping("/hello")
	public String hello(
			@RequestParam(value = "name", required = false, defaultValue = "World!") String name,
			Model model) {
		String message = demo.getMessage();//"Hello " + name;

		// store message as a model attribute
		model.addAttribute("message", message);

		return "hello";// forward request to WEB-INF/views/hello.jsp
	}
}
