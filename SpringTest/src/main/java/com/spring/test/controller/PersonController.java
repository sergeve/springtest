package com.spring.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.spring.test.model.PersonEntity;
import com.spring.test.service.PersonService;

@Controller
@RequestMapping(value = "/person")
public class PersonController {

	@Autowired 
	PersonService personService;
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody PersonEntity addperson() {
		PersonEntity person = new PersonEntity();

		return personService.savePerson(person);
	}

	@RequestMapping(value = "/getPersons", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<PersonEntity> getPersons() {
		return personService.getPersons();
	}
}
